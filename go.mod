module gitlab.com/slagit/vault/config

go 1.15

require (
	github.com/hashicorp/go-cleanhttp v0.5.2
	github.com/hashicorp/hcl/v2 v2.9.0
	github.com/hashicorp/vault/api v1.0.4
	github.com/zclconf/go-cty v1.8.0
)
