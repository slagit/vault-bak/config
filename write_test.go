package config

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"net"
	"net/http"
	"os"
	"testing"
	"time"
)

func TestWriteNoSeal(t *testing.T) {
	cert, pk := createKeyPair(t)
	w, m := createWriter(t)
	if err := w.Write("test.example.com", &UserData{
		Vault: VaultData{
			APIAddr: "http://example.com",
			TLS: TLSData{
				Certificate: cert,
				PrivateKey:  pk,
			},
		},
	}); err != nil {
		t.Errorf("%s", err)
	}
	if m["/etc/vault/server.pem"] != cert {
		t.Errorf("incorrect certificate in /etc/vault/server.pem")
	}
	if m["/etc/vault/server-key.pem"] != pk {
		t.Errorf("incorrect key in /etc/vault/server-key.pem")
	}
	if m["/etc/vault/config.hcl"] != `api_addr      = "http://example.com"
cluster_addr  = "https://192.168.1.21:8201"
disable_mlock = true
pid_file      = "/var/run/vault/vault.pid"

backend "raft" {
  path    = "/var/lib/vault"
  node_id = "test.example.com"
}

# public listener for direct access
listener "tcp" {
  address                  = "10.1.1.37:443"
  tls_cert_file            = "/etc/vault/server.pem"
  tls_key_file             = "/etc/vault/server-key.pem"
  tls_disable_client_certs = true
}

# private listener for load balancer and cluster access
listener "tcp" {
  address                  = "192.168.1.21:443"
  cluster_address          = "192.168.1.21:8201"
  proxy_protocol_behavior  = "use_always"
  tls_cert_file            = "/etc/vault/server.pem"
  tls_key_file             = "/etc/vault/server-key.pem"
  tls_disable_client_certs = true
}

# private listener for unencrypted load balancer status checks
listener "tcp" {
  address                 = "192.168.1.21:80"
  proxy_protocol_behavior = "use_always"
  tls_disable             = true
}
` {
		t.Errorf("incorrect configuration in /etc/vault/config.hcl")
	}
}

func TestWriteSeal(t *testing.T) {
	ln, a, wt := createVault(t)
	defer ln.Close()

	cert, pk := createKeyPair(t)
	w, m := createWriter(t)
	if err := w.Write("test.example.com", &UserData{
		Vault: VaultData{
			APIAddr: "http://example.com",
			Seal: &SealData{
				Address:      a,
				KeyName:      "my_key",
				MountPath:    "path/to/transit",
				WrappedToken: wt,
			},
			TLS: TLSData{
				Certificate: cert,
				PrivateKey:  pk,
			},
		},
	}); err != nil {
		t.Errorf("%s", err)
	}
	if m["/etc/vault/server.pem"] != cert {
		t.Errorf("incorrect certificate in /etc/vault/server.pem")
	}
	if m["/etc/vault/server-key.pem"] != pk {
		t.Errorf("incorrect key in /etc/vault/server-key.pem")
	}
	if m["/etc/vault/config.hcl"] != `api_addr      = "http://example.com"
cluster_addr  = "https://192.168.1.21:8201"
disable_mlock = true
pid_file      = "/var/run/vault/vault.pid"

backend "raft" {
  path    = "/var/lib/vault"
  node_id = "test.example.com"
}

# public listener for direct access
listener "tcp" {
  address                  = "10.1.1.37:443"
  tls_cert_file            = "/etc/vault/server.pem"
  tls_key_file             = "/etc/vault/server-key.pem"
  tls_disable_client_certs = true
}

# private listener for load balancer and cluster access
listener "tcp" {
  address                  = "192.168.1.21:443"
  cluster_address          = "192.168.1.21:8201"
  proxy_protocol_behavior  = "use_always"
  tls_cert_file            = "/etc/vault/server.pem"
  tls_key_file             = "/etc/vault/server-key.pem"
  tls_disable_client_certs = true
}

# private listener for unencrypted load balancer status checks
listener "tcp" {
  address                 = "192.168.1.21:80"
  proxy_protocol_behavior = "use_always"
  tls_disable             = true
}

seal "transit" {
  address    = "http://127.0.0.1:9000"
  key_name   = "my_key"
  mount_path = "path/to/transit"
  token      = "test_token"
}
` {
		t.Errorf("incorrect configuration in /etc/vault/config.hcl")
	}
}

func createKeyPair(t *testing.T) (string, string) {
	t.Helper()

	pk, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	derPK, err := x509.MarshalPKCS8PrivateKey(pk)
	if err != nil {
		t.Fatal(err)
	}
	pemPK := &bytes.Buffer{}
	if err := pem.Encode(pemPK, &pem.Block{Type: "PRIVATE KEY", Bytes: derPK}); err != nil {
		t.Fatal(err)
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			CommonName: "Test Keypair",
		},
		NotBefore: time.Now().Add(time.Second * -10),
		NotAfter:  time.Now().Add(time.Hour * 24),
	}

	derCert, err := x509.CreateCertificate(rand.Reader, &template, &template, &pk.PublicKey, pk)
	if err != nil {
		t.Fatal(err)
	}

	pemCert := &bytes.Buffer{}
	if err := pem.Encode(pemCert, &pem.Block{Type: "CERTIFICATE", Bytes: derCert}); err != nil {
		t.Fatal(err)
	}

	return pemCert.String(), pemPK.String()
}

func createVault(t *testing.T) (net.Listener, string, string) {
	t.Helper()

	ln, err := net.Listen("tcp", "127.0.0.1:9000")
	if err != nil {
		t.Fatal(err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/v1/sys/wrapping/unwrap", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("{\"auth\":{\"client_token\":\"test_token\"}}"))
	})

	server := &http.Server{
		Addr:    ln.Addr().String(),
		Handler: mux,
	}
	go server.Serve(ln)

	addr := "http://" + ln.Addr().String()

	return ln, addr, "test_wrapped_token"
}

type mapWriter map[string]string

func (mw mapWriter) WriteFile(filename string, data []byte, _ string, _ os.FileMode) error {
	mw[filename] = string(data)
	return nil
}

func createWriter(t *testing.T) (*Writer, map[string]string) {
	t.Helper()

	mw := make(mapWriter)
	w := NewWriter(mw)
	w.PrivateIP = "192.168.1.21"
	w.PublicIP = "10.1.1.37"

	return w, mw
}
