package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"strconv"
	"strings"

	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/hashicorp/vault/api"
	"github.com/zclconf/go-cty/cty"
)

type FileWriter interface {
	WriteFile(string, []byte, string, os.FileMode) error
}

type fsFileWriter struct {
}

func (fw *fsFileWriter) WriteFile(filename string, data []byte, groupName string, perm os.FileMode) error {
	log.Printf("Writing %s...", filename)
	groupStr, err := user.LookupGroup(groupName)
	if err != nil {
		return err
	}
	group, err := strconv.Atoi(groupStr.Gid)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(filename, data, perm); err != nil {
		return err
	}

	if err := os.Chown(filename, 0, group); err != nil {
		return err
	}
	return nil
}

func Filesystem() FileWriter {
	return &fsFileWriter{}
}

type Writer struct {
	FileWriter
	ConfigRootPath string
	GroupName      string
	ManageHostname bool
	PIDPath        string
	PrivateIP      string
	PublicIP       string
	RaftPath       string
}

func NewWriter(fw FileWriter) *Writer {
	return &Writer{
		FileWriter:     fw,
		ConfigRootPath: "/etc/vault",
		GroupName:      "vault",
		PIDPath:        "/var/run/vault/vault.pid",
		RaftPath:       "/var/lib/vault",
	}
}

func (w *Writer) Write(hostname string, userData *UserData) error {
	if err := userData.Validate(nil); err != nil {
		return err
	}

	certPath := path.Join(w.ConfigRootPath, "server.pem")
	keyPath := path.Join(w.ConfigRootPath, "server-key.pem")
	sealCAPath := path.Join(w.ConfigRootPath, "seal-ca.pem")
	configPath := path.Join(w.ConfigRootPath, "config.hcl")

	if w.ManageHostname {
		// TODO: Configurable path?
		if err := w.WriteFile("/etc/hostname", []byte(hostname), "root", 0444); err != nil {
			return err
		}
	}

	if err := w.WriteFile(certPath, []byte(userData.Vault.TLS.Certificate), w.GroupName, 0440); err != nil {
		return err
	}

	if err := w.WriteFile(keyPath, []byte(userData.Vault.TLS.PrivateKey), w.GroupName, 0440); err != nil {
		return err
	}

	var sealToken string
	if userData.Vault.Seal != nil {
		if userData.Vault.Seal.CACertificate != "" {
			if err := w.WriteFile(sealCAPath, []byte(userData.Vault.Seal.CACertificate), w.GroupName, 0440); err != nil {
				return err
			}
		}

		var err error
		sealToken, err = unwrapToken(userData.Vault.Seal, sealCAPath)
		if err != nil {
			return err
		}
	}

	f := hclwrite.NewFile()
	f.Body().SetAttributeValue("api_addr", cty.StringVal(userData.Vault.APIAddr))
	f.Body().SetAttributeValue("cluster_addr", cty.StringVal(fmt.Sprintf("https://%s:8201", w.PrivateIP)))
	f.Body().SetAttributeValue("disable_mlock", cty.BoolVal(true))
	f.Body().SetAttributeValue("pid_file", cty.StringVal(w.PIDPath))

	f.Body().AppendNewline()
	raft := f.Body().AppendNewBlock("backend", []string{"raft"})
	raft.Body().SetAttributeValue("path", cty.StringVal(w.RaftPath))
	raft.Body().SetAttributeValue("node_id", cty.StringVal(hostname))

	f.Body().AppendNewline()
	addComment(f.Body(), "public listener for direct access")
	tcp1 := f.Body().AppendNewBlock("listener", []string{"tcp"})
	tcp1.Body().SetAttributeValue("address", cty.StringVal(fmt.Sprintf("%s:443", w.PublicIP)))
	tcp1.Body().SetAttributeValue("tls_cert_file", cty.StringVal(certPath))
	tcp1.Body().SetAttributeValue("tls_key_file", cty.StringVal(keyPath))
	tcp1.Body().SetAttributeValue("tls_disable_client_certs", cty.True)

	f.Body().AppendNewline()
	addComment(f.Body(), "private listener for load balancer and cluster access")
	tcp2 := f.Body().AppendNewBlock("listener", []string{"tcp"})
	tcp2.Body().SetAttributeValue("address", cty.StringVal(fmt.Sprintf("%s:443", w.PrivateIP)))
	tcp2.Body().SetAttributeValue("cluster_address", cty.StringVal(fmt.Sprintf("%s:8201", w.PrivateIP)))
	tcp2.Body().SetAttributeValue("proxy_protocol_behavior", cty.StringVal("use_always"))
	tcp2.Body().SetAttributeValue("tls_cert_file", cty.StringVal(certPath))
	tcp2.Body().SetAttributeValue("tls_key_file", cty.StringVal(keyPath))
	tcp2.Body().SetAttributeValue("tls_disable_client_certs", cty.True)

	f.Body().AppendNewline()
	addComment(f.Body(), "private listener for unencrypted load balancer status checks")
	tcp3 := f.Body().AppendNewBlock("listener", []string{"tcp"})
	tcp3.Body().SetAttributeValue("address", cty.StringVal(fmt.Sprintf("%s:80", w.PrivateIP)))
	tcp3.Body().SetAttributeValue("proxy_protocol_behavior", cty.StringVal("use_always"))
	tcp3.Body().SetAttributeValue("tls_disable", cty.True) // TODO: Not needed anymore?

	if userData.Vault.Seal != nil {
		f.Body().AppendNewline()
		seal := f.Body().AppendNewBlock("seal", []string{"transit"})
		seal.Body().SetAttributeValue("address", cty.StringVal(userData.Vault.Seal.Address))
		seal.Body().SetAttributeValue("key_name", cty.StringVal(userData.Vault.Seal.KeyName))
		seal.Body().SetAttributeValue("mount_path", cty.StringVal(userData.Vault.Seal.MountPath))
		seal.Body().SetAttributeValue("token", cty.StringVal(sealToken))
		if userData.Vault.Seal.CACertificate != "" {
			seal.Body().SetAttributeValue("tls_ca_cert", cty.StringVal(sealCAPath))
		}
	}

	if err := w.WriteFile(configPath, f.Bytes(), w.GroupName, 0440); err != nil {
		return err
	}
	return nil
}

func addComment(b *hclwrite.Body, c string) {
	for _, l := range strings.Split(c, "\n") {
		b.AppendUnstructuredTokens(hclwrite.Tokens{
			{
				Type:  hclsyntax.TokenComment,
				Bytes: []byte(fmt.Sprintf("# %s\n", l)),
			},
		})
	}
}

func unwrapToken(seal *SealData, caPath string) (string, error) {
	log.Println("Unwrapping seal vault token...")
	cfg := api.DefaultConfig()
	cfg.Address = seal.Address
	if seal.CACertificate != "" {
		if err := cfg.ConfigureTLS(&api.TLSConfig{
			CACert: caPath,
		}); err != nil {
			return "", err
		}
	}

	client, err := api.NewClient(cfg)
	if err != nil {
		return "", err
	}
	client.SetToken(seal.WrappedToken)

	sec, err := client.Logical().Unwrap(seal.WrappedToken)
	if err != nil {
		return "", err
	}

	if sec.Auth == nil {
		return "", errors.New("No token found in unwrapped data")
	}

	return sec.Auth.ClientToken, nil
}
