package config

import (
	"bytes"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"net/url"
	"strings"
)

type ValidationPath []string

func (vp ValidationPath) Error(msg string) error {
	p := strings.Join(vp, ".")
	return errors.New(fmt.Sprintf("%s %s", p, msg))
}

func (vp ValidationPath) Missing() error {
	return vp.Error("must be specified")
}

type SealData struct {
	Address       string `json:"address"`
	CACertificate string `json:"tls_ca_cert,omitempty"`
	KeyName       string `json:"key_name"`
	MountPath     string `json:"mount_path"`
	WrappedToken  string `json:"wrapped_token"`
}

func (d *SealData) Validate(path ValidationPath) error {
	if err := validateAddress(append(path, "address"), d.Address); err != nil {
		return err
	}

	if d.CACertificate != "" {
		if certs, err := validateCertificates(append(path, "tls_ca_cert"), d.CACertificate); err != nil {
			return fmt.Errorf("%s if specified", err.Error())
		} else {
			d.CACertificate = string(certs)
		}
	}

	if d.KeyName == "" {
		return append(path, "key_name").Missing()
	}

	if strings.Contains(d.KeyName, "/") {
		return append(path, "key_name").Error("cannot contain '/'")
	}

	if d.MountPath == "" {
		return append(path, "mount_path").Missing()
	}

	if d.WrappedToken == "" {
		return append(path, "wrapped_token").Missing()
	}

	return nil
}

type TLSData struct {
	Certificate string `json:"cert"`
	PrivateKey  string `json:"key"`
}

func (d *TLSData) Validate(path ValidationPath) error {
	if certs, err := validateCertificates(append(path, "cert"), d.Certificate); err != nil {
		return err
	} else {
		d.Certificate = string(certs)
	}

	if key, err := validatePrivateKey(append(path, "key"), d.PrivateKey); err != nil {
		return err
	} else {
		d.PrivateKey = string(key)
	}

	return nil
}

type VaultData struct {
	APIAddr string    `json:"api_addr"`
	Seal    *SealData `json:"seal,omitempty"`
	TLS     TLSData   `json:"tls"`
}

func (d *VaultData) Validate(path ValidationPath) error {
	if err := validateAddress(append(path, "api_addr"), d.APIAddr); err != nil {
		return err
	}

	if d.Seal != nil {
		if err := d.Seal.Validate(append(path, "seal")); err != nil {
			return err
		}
	}

	return d.TLS.Validate(append(path, "tls"))
}

type UserData struct {
	Vault VaultData `json:"vault"`
}

func (d *UserData) Validate(path ValidationPath) error {
	return d.Vault.Validate(append(path, "vault"))
}

type validateCryptoOptions struct {
	Type  string
	Func  func([]byte) error
	Limit int
	Path  ValidationPath
}

func validateCrypto(opts *validateCryptoOptions, value string) ([]byte, error) {
	data := []byte(value)
	count := 0
	var result bytes.Buffer

	if len(data) == 0 {
		return nil, opts.Path.Missing()
	}

	for {
		var blk *pem.Block
		blk, data = pem.Decode(data)
		if blk == nil {
			break
		}

		if blk.Type != opts.Type {
			continue
		}

		if err := opts.Func(blk.Bytes); err != nil {
			return nil, opts.Path.Error(err.Error())
		} else {
			if err := pem.Encode(&result, blk); err != nil {
				return nil, opts.Path.Error(err.Error())
			}
		}

		count += 1
		if opts.Limit != 0 && count >= opts.Limit {
			break
		}
	}

	if count == 0 {
		return nil, opts.Path.Error("must contain at least one valid entry")
	}

	return result.Bytes(), nil
}

func validateCertificates(path ValidationPath, certs string) ([]byte, error) {
	return validateCrypto(&validateCryptoOptions{
		Type: "CERTIFICATE",
		Func: func(d []byte) error {
			_, err := x509.ParseCertificate(d)
			if err != nil {
				return err
			}
			return nil
		},
		Path: path,
	}, certs)
}

func validatePrivateKey(path ValidationPath, certs string) ([]byte, error) {
	return validateCrypto(&validateCryptoOptions{
		Type: "PRIVATE KEY",
		Func: func(d []byte) error {
			_, err := x509.ParsePKCS8PrivateKey(d)
			if err != nil {
				return err
			}
			return nil
		},
		Limit: 1,
		Path:  path,
	}, certs)
}

func validateAddress(path ValidationPath, u string) error {
	if u == "" {
		return path.Missing()
	}

	if _, err := url.Parse(u); err != nil {
		return path.Error(err.Error())
	}

	return nil
}
