package config

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/hashicorp/go-cleanhttp"
)

type Endpoint struct {
	HttpClient *http.Client
	URL        *url.URL
}

func NewEndpoint(ep string) (*Endpoint, error) {
	u, err := url.Parse(ep)
	if err != nil {
		return nil, err
	}
	c := cleanhttp.DefaultClient()

	return &Endpoint{
		URL:        u,
		HttpClient: c,
	}, nil
}

func (ep *Endpoint) url(name string) *url.URL {
	n, err := url.Parse(name)
	if err != nil {
		panic(err)
	}
	return ep.URL.ResolveReference(n)
}

func (ep *Endpoint) metadata(name string) ([]byte, error) {
	u := ep.url(name)

	res, err := ep.HttpClient.Get(u.String())
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Received code %d while querying %s", res.StatusCode, name)
	}

	return ioutil.ReadAll(res.Body)
}

func (ep *Endpoint) Hostname() (string, error) {
	m, err := ep.metadata("hostname")
	if err != nil {
		return "", err
	}
	return string(m), nil
}

func (ep *Endpoint) UserData() ([]byte, error) {
	return ep.metadata("user-data")
}
