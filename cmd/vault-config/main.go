package main

import (
	"flag"
	"fmt"
	"net"

	"gitlab.com/slagit/vault/config"
)

func getPrimaryAddress(ifname string) (string, error) {
	iface, err := net.InterfaceByName(ifname)
	if err != nil {
		return "", err
	}

	addrs, err := iface.Addrs()
	if err != nil {
		return "", err
	}

	if len(addrs) == 0 {
		return "", fmt.Errorf("No address found for interface %s", ifname)
	}

	ip, _, err := net.ParseCIDR(addrs[0].String())
	if err != nil {
		return "", err
	}

	return ip.String(), nil
}

func main() {
	w := config.NewWriter(config.Filesystem())
	flag.StringVar(&w.GroupName, w.GroupName, "vault", "group ownership for vault configuration")
	endpoint := flag.String("endpoint", "http://169.254.169.254/metadata/v1/", "metadata service endpoint")
	flag.StringVar(&w.ConfigRootPath, "path", w.ConfigRootPath, "path to primary configuration files")
	flag.StringVar(&w.PIDPath, "pid-file", w.PIDPath, "path to pid file")
	privIf := flag.String("private-iface", "eth1", "private network interface (used if --private-ip not set)")
	flag.StringVar(&w.PrivateIP, "private-ip", w.PrivateIP, "private IP address")
	pubIf := flag.String("public-iface", "eth0", "public network interface (used if --public-ip not set)")
	flag.StringVar(&w.PublicIP, "public-ip", w.PublicIP, "public IP address")
	flag.StringVar(&w.RaftPath, "raft-path", w.RaftPath, "path to raft database")
	flag.Parse()

	if w.PublicIP == "" {
		var err error
		w.PublicIP, err = getPrimaryAddress(*pubIf)
		if err != nil {
			panic(err)
		}
	}

	if w.PrivateIP == "" {
		var err error
		w.PrivateIP, err = getPrimaryAddress(*privIf)
		if err != nil {
			panic(err)
		}
	}

	ep, err := config.NewEndpoint(*endpoint)
	if err != nil {
		panic(err)
	}

	hostname, err := ep.Hostname()
	if err != nil {
		panic(err)
	}

	rawUserData, err := ep.UserData()
	if err != nil {
		panic(err)
	}

	userData, err := config.Parse(rawUserData)
	if err != nil {
		panic(err)
	}

	if err := w.Write(hostname, userData); err != nil {
		panic(err)
	}
}
