include:
- template: SAST.gitlab-ci.yml
- template: Secret-Detection.gitlab-ci.yml
- template: Dependency-Scanning.gitlab-ci.yml

stages:
- test
- build
- package
- release

variables:
  CGO_ENABLED: '0'

workflow:
  rules:
  - if: $CI_MERGE_REQUEST_IID
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

gemnasium-dependency_scanning:
  interruptible: true
  rules:
  - if: $CI_MERGE_REQUEST_IID
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

gosec-sast:
  interruptible: true
  rules:
  - if: $CI_MERGE_REQUEST_IID
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

secret_detection:
  interruptible: true
  rules:
  - if: $CI_MERGE_REQUEST_IID

format:
  image: docker.io/library/golang:1.16.0-alpine@sha256:a459519e1a420c9ac6c9c8442994fb16cdf5790b87da8dfab8cf83898aee6fb3
  interruptible: true
  needs: []
  script:
  - test -z "$(gofmt -s -l $(find . -name '*.go' -type f -print) | tee /dev/stderr)"
  stage: test

test:
  image: docker.io/library/golang:1.16.0-alpine@sha256:a459519e1a420c9ac6c9c8442994fb16cdf5790b87da8dfab8cf83898aee6fb3
  interruptible: true
  needs: []
  script:
  - go test $(go list ./...)
  stage: test

vet:
  image: docker.io/library/golang:1.16.0-alpine@sha256:a459519e1a420c9ac6c9c8442994fb16cdf5790b87da8dfab8cf83898aee6fb3
  interruptible: true
  needs: []
  script:
  - go vet $(go list ./...)
  stage: test

build:
  artifacts:
    expire_in: 1 hour
    paths:
    - vault-config
  image: docker.io/library/golang:1.16.0-alpine@sha256:a459519e1a420c9ac6c9c8442994fb16cdf5790b87da8dfab8cf83898aee6fb3
  interruptible: true
  needs: []
  script:
  - go build -mod=readonly -o vault-config ./cmd/vault-config
  stage: build
  variables:
    GOARCH: amd64
    GOOS: linux

package:
  artifacts:
    expire_in: 1 hour
    paths:
    - vault-config-*.zip
  dependencies:
  - build
  image: docker.io/library/alpine:3.13.2@sha256:4661fb57f7890b9145907a1fe2555091d333ff3d28db86c3bb906f6a2be93c87
  interruptible: true
  needs:
  - build
  script:
  - apk add zip
  - zip -9 vault-config-$GOOS-$GOARCH.zip vault-config
  stage: package
  variables:
    GOARCH: amd64
    GOOS: linux

release:
  dependencies:
  - package
  image: docker.io/library/node:14.16.0-alpine@sha256:834bd64f2285b709c44d6ca0e06294a9ea3f224967d02342cd155bbe98cdcbd3
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
  - apk add --no-cache git
  - npm install semantic-release @semantic-release/exec @semantic-release/gitlab
  - npx semantic-release
  stage: release
