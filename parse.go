package config

import (
	"encoding/json"
)

func Parse(data []byte) (*UserData, error) {
	var ud UserData

	err := json.Unmarshal(data, &ud)
	if err != nil {
		return nil, err
	}

	return &ud, nil
}
