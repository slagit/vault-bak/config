#!/bin/sh

set -eu

VERSION=$1

handle() {
    mv "vault-config-$1.zip" "vault-config-$VERSION-$1.zip"
    sha256sum "vault-config-$VERSION-$1.zip" > "vault-config-$VERSION-$1.zip.sha256"
}

handle linux-amd64
